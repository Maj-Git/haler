import {createAsyncThunk, createSelector, createSlice, PayloadAction} from "@reduxjs/toolkit";
import axios, {AxiosError, AxiosHeaders} from "axios";
import {AppDispatch, RootState} from "./store.ts";
import {findResource, isResource, Resource} from "../core/models/resource.ts";

type LoadResourceError = {
    message: string;
}

export interface ResourcesState {
    root?: Resource,
    selectedResourceId?: string,
    authorization: string | null,
    lastError?: LoadResourceError
}

// Define the initial state using that type
const initialState: ResourcesState = {authorization: null}

const selectSelf = (state: RootState) => state.resources;
export const selectRoot = createSelector(selectSelf, state => state.root);
export const selectedResourceId = createSelector(selectSelf, state => state.selectedResourceId);
export const selectLastError = createSelector(selectSelf, state => state.lastError);

export const loadResource = createAsyncThunk<
    Resource,
    Pick<Resource, 'url'> & { authorization: string } | Resource,
    {
        dispatch: AppDispatch,
        state: RootState,
        rejectValue: LoadResourceError
    }>(
    'resources/fetchResource',
    async (urlOrResource, {dispatch, getState, rejectWithValue}) => {

        const {url} = urlOrResource;

        // We're fetching a new root, reset the authorization.
        if (!isResource(urlOrResource)) {

            if (urlOrResource.authorization.trim().length > 0) {
                dispatch(setAuthorization({authorization: urlOrResource.authorization}))
            } else {
                dispatch(clearAuthorization());
            }
        }

        // Resource is already loaded, just return it for selection.
        if (isResource(urlOrResource) && urlOrResource.content) {
            return urlOrResource;
        }

        const requestHeaders = new AxiosHeaders();

        const {resources} = getState();
        if (resources.authorization != null) {
            requestHeaders.setAuthorization(resources.authorization);
        }

        try {

            const {headers, data} = await axios.get(url, {headers: requestHeaders});
            const contentType = headers["content-type"] as string;

            if (!/json/i.test(contentType)) {
                const message = `Content type ${contentType} is not supported.`;

                return rejectWithValue({message})
            }

            const content = JSON.stringify(data, null, 2);
            const json = JSON.parse(content);
            const links = json['_links'] as {
                [p: string]: {
                    href: string
                }
            };

            const resource: Omit<Resource, 'relations'> = !isResource(urlOrResource) ?
                {
                    id: 'root',
                    displayName: 'root',
                    url: url,
                    content
                } :
                {
                    ...urlOrResource,
                    content
                };

            const relations: Resource[] = links ? Object.entries(links)
                .filter(([rel]) => rel !== 'self') // ignore self relation
                .map(([rel, {href}]) => ({
                    id: resource.id + '->' + rel,
                    displayName: rel,
                    url: href,
                    relations: [],
                })) : [];

            return {...resource, relations};

        } catch (error) {
            const message = error instanceof AxiosError ? error.message : "";
            return rejectWithValue({message});
        }
    })
;

export const resourcesSlice = createSlice({
    name: 'resources',
    initialState,
    reducers: {
        setAuthorization: (state: ResourcesState, {payload}: PayloadAction<{
            authorization: string
        }>) => {
            state.authorization = payload.authorization;
        },
        clearAuthorization: (state: ResourcesState) => {
            state.authorization = null
        },
    },
    extraReducers: builder => {
        builder
            .addCase(loadResource.rejected, (state, {payload}) => {
                state.lastError = payload;
            })
            .addCase(loadResource.fulfilled, (state, {payload}) => {
                if (payload.id === 'root') {
                    state.root = payload;
                } else {
                    const res = findResource(state.root!, payload.id);

                    if (res) {
                        res.content = payload.content;
                        res.relations = payload.relations;
                    }
                }

                state.selectedResourceId = payload.id;
            });
    }
});

const {setAuthorization, clearAuthorization} = resourcesSlice.actions;

export default resourcesSlice.reducer