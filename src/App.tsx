import './App.css'
import 'reactflow/dist/style.css';
import {Sidebar} from "./core/components/sidebar.tsx";
import {ResourcesFlow} from "./core/components/resources-flow.tsx";
import {useCallback, useEffect, useState} from "react";
import {SidebarToggle} from "./core/components/sidebar-toggle.tsx";
import {Snackbar} from "@mui/joy";
import {useAppSelector} from "./core/hooks.ts";
import {selectLastError} from "./store/resources-slice.ts";


const App = () => {

    const [isSidebarVisible, setIsSidebarVisible] = useState(true);
    const [isErrorSnackbarVisible, setIsErrorSnackbarVisible] = useState(false);

    const lastResourceError = useAppSelector(selectLastError);

    const hideSidebar = useCallback(() => {
        setIsSidebarVisible(false)
    }, []);

    const showSidebar = useCallback(() => {
        setIsSidebarVisible(true);
    }, []);

    useEffect(() => {
        if (lastResourceError) {
            setIsErrorSnackbarVisible(true);
        }
    }, [lastResourceError]);

    return (
        <div className="main-container">
            <div onMouseLeave={hideSidebar}>
                <Sidebar visible={isSidebarVisible}></Sidebar>
            </div>
            <div onMouseEnter={showSidebar}>
                <SidebarToggle></SidebarToggle>
            </div>
            <ResourcesFlow></ResourcesFlow>

            <Snackbar
                data-cy="error_snackbar"
                color="danger"
                autoHideDuration={5000}
                anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
                open={isErrorSnackbarVisible}
                onClose={() => setIsErrorSnackbarVisible(false)}>
                {lastResourceError?.message}
            </Snackbar>
        </div>
    )
};

export default App;


