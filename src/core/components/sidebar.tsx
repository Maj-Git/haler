import {Button, Divider, Input, List, Stack} from "@mui/joy";
import {useCallback, useState} from "react";
import {useAppDispatch, useAppSelector} from "../hooks.ts";
import {loadResource} from "../../store/resources-slice.ts";
import {ResourceList} from "./resource-list.tsx";

type SidebarProps = {
    visible: boolean
};
export const Sidebar = ({visible}: SidebarProps) => {

    const [url, setUrl] = useState('');
    const [authorization, setAuthorization] = useState('');
    const dispatch = useAppDispatch();
    const root = useAppSelector(state => state.resources.root);

    const execute = useCallback(() => {
        dispatch(loadResource({url, authorization}));
        setUrl('');
        setAuthorization('')
    }, [dispatch, url, authorization])

    return (
        <div className={visible ? "sidebar visible" : "sidebar"}>
            <Stack spacing={2} direction="row">
                <Input data-cy="url-input" placeholder="URL, cURL, ..." variant="plain"
                       value={url}
                       onChange={event => setUrl(event.target.value)}/>
                <Button data-cy="execute" type="submit" onClick={execute}>Execute</Button>
            </Stack>

            <Divider style={{marginTop: "10px", marginBottom: "10px"}}/>

            <Input placeholder="Authorization" variant="plain"
                   value={authorization}
                   onChange={event => setAuthorization(event.target.value)}/>

            <Divider style={{marginTop: "10px", marginBottom: "10px"}}/>

            {
                !root ? null :
                    <List>
                        <ResourceList resource={root}
                                      loadResource={(resource) => dispatch(loadResource(resource))}></ResourceList>
                    </List>
            }
        </div>
    );
}






