import {List, ListItem, ListItemButton, ListItemContent} from "@mui/joy";
import {Resource} from "../models/resource.ts";

type ResourceListProps = {
    resource: Resource,
    loadResource: (resource: Resource) => void
}
export const ResourceList = ({resource, loadResource}: ResourceListProps) => (
    <div data-cy={resource.displayName}>
        <ListItem>
            <ListItemButton onClick={() => loadResource(resource)}>
                <ListItemContent>
                    {resource.displayName}
                </ListItemContent>
            </ListItemButton>
        </ListItem>
        {
            resource.relations.length === 0 ? null :
                <List style={{paddingLeft: "10px"}}>
                    {
                        resource.relations.map(res => <ResourceList loadResource={loadResource} key={res.id}
                                                                    resource={res}/>)
                    }
                </List>
        }
    </div>
);