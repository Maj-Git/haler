import SyntaxHighlighter from "react-syntax-highlighter";
import {dark} from "react-syntax-highlighter/dist/cjs/styles/hljs";
import {Handle, NodeProps, Position} from "reactflow";
import {NODE_HEIGHT, NODE_WIDTH} from "../constants.ts";
import {Component} from "react";

type Props = {
    resource: string
};

export class ResourceNode extends Component<NodeProps<Props>> {
    render() {
        const {data} = this.props;
        return (
            <div>
                <Handle id="right" type="source" position={Position.Right}/>
                <SyntaxHighlighter wrapLongLines language="json" style={dark} customStyle={{
                    height: NODE_HEIGHT,
                    width: NODE_WIDTH,
                    overflowY: "hidden"
                }}>
                    {data.resource}
                </SyntaxHighlighter>
                <Handle id="left" type="target" position={Position.Left}/>
            </div>
        )
    }
}
