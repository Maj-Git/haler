import {
    Edge,
    MarkerType,
    Node,
    NodeTypes,
    ReactFlow,
    useEdgesState,
    useNodesState,
    useReactFlow,
    useViewport,
    XYPosition
} from "reactflow";
import {ResourceNode} from "./resource-node.tsx";
import React, {MouseEvent as ReactMouseEvent, useCallback, useEffect} from "react";
import {selectedResourceId, selectRoot} from "../../store/resources-slice.ts";
import {useAppSelector} from "../hooks.ts";
import {Resource} from "../models/resource.ts";
import {NODE_HEIGHT, NODE_WIDTH} from "../constants.ts";
import {Box, Modal} from "@mui/joy";
import {dark} from "react-syntax-highlighter/dist/cjs/styles/hljs";
import SyntaxHighlighter from "react-syntax-highlighter";

type NodesAndEdges = { nodes: Node[], edges: Edge[] };

const nodeTypes: NodeTypes = {
    resource: ResourceNode
};

const style = {
    position: 'absolute',
    top: '50%',
    right: '0',
    transform: 'translate(0, -50%)',
    boxShadow: 24,
    p: 4,
    overflowX: "auto",
    maxHeight: "100vh"
};

/**
 * Starting from the provided root and initial position, gets the nodes and edges to lay out with their computed positions.
 * @param resource The root resource.
 * @param x Position x-axis.
 * @param y Position y-axis.
 * @return The nodes (with their computed positions) and edges linking nodes.
 */
const getLayout = (resource: Resource, {x, y}: XYPosition): NodesAndEdges => {

    const nodes: Node[] = [];
    const edges: Edge[] = [];

    const node: Node = {
        id: resource.id,
        type: 'resource',
        data: {resource: resource.content},
        position: {x, y}
    }

    nodes.push(node);

    let currentY = y;
    for (const rel of resource.relations.filter(r => r.content)) {
        const split = rel.id.split('->');

        const edge: Edge = {
            id: rel.id,
            source: resource.id,
            target: rel.id,
            label: split[split.length - 1],
            markerEnd: {type: MarkerType.Arrow, width: 50, height: 25},
            labelStyle: {fontSize: '20px'},
            labelBgPadding: [20, 10],
            style: {stroke: '#fff'},
        };

        edges.push(edge);

        const {nodes: relNodes, edges: relEdges} = getLayout(rel, {x: x + NODE_WIDTH + 200, y: currentY});
        relNodes.forEach(n => nodes.push(n));
        relEdges.forEach(e => edges.push(e));

        currentY = currentY + NODE_HEIGHT + 100;
    }

    return {nodes, edges};
}

export const ResourcesFlow = () => {
    const {zoom} = useViewport();
    const {setCenter} = useReactFlow();
    const [nodes, setNodes, onNodesChange] = useNodesState([]);
    const [edges, setEdges, onEdgesChange] = useEdgesState([]);

    const [expandedResource, setExpandedResource] = React.useState<string | null>();

    const root = useAppSelector(selectRoot);
    const selectedResource = useAppSelector(selectedResourceId);

    // Center the position on the provided node.
    const centerPosition = useCallback(({position}: Node) => {
        const {x, y} = {
            x: position.x + (NODE_WIDTH! / 2), y: position.y + (NODE_HEIGHT! / 2)
        }
        setCenter(x, y, {duration: 500, zoom});
    }, [setCenter, zoom]);

    // Center position when a node is loaded/selected.
    useEffect(() => {
        const node = nodes.find(node => node.id === selectedResource);

        if (node) {
            centerPosition(node);
        }
    }, [nodes.length, selectedResource]);

    useEffect(() => {
        if (!root) {
            setNodes([]);
            setEdges([]);
        } else {
            const {nodes, edges} = getLayout(root, {x: 0, y: 0});

            setNodes([...nodes]);
            setEdges([...edges]);
        }
    }, [root, setEdges, setNodes]);

    const expandResourceNode = useCallback((_: ReactMouseEvent, node: Node) => {
        if (node.type === 'resource') {
            setExpandedResource(node.data.resource);
        }
    }, []);

    return (
        <>
            <ReactFlow
                nodes={nodes}
                edges={edges}
                onNodesChange={onNodesChange}
                onEdgesChange={onEdgesChange}
                onNodeDoubleClick={expandResourceNode}
                nodeTypes={nodeTypes}
                fitView></ReactFlow>
            <Modal
                open={expandedResource != null}
                onClose={() => setExpandedResource(null)}
                aria-labelledby="modal-modal-title"
                aria-describedby="modal-modal-description"
            >
                <Box sx={style}>
                    <SyntaxHighlighter wrapLongLines language="json" style={dark}>
                        {expandedResource!}
                    </SyntaxHighlighter>
                </Box>
            </Modal>
        </>
    );
}