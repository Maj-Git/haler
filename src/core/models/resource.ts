/**
 * Represents a resource with a loadable content and related resources.
 */
export interface Resource {
    id: string;
    displayName: string;
    url: string;
    content?: string,
    relations: Resource[]
}

/**
 * Resource type guard;
 * @param url An object that at least contains an url.
 */
export const isResource = (url: Pick<Resource, 'url'>): url is Resource => {
    return 'id' in url;
}

/**
 * Starting from the provided root resource and recursing through relations if required, try to find
 * the resource with the provided id.
 * @param resource The root resource.
 * @param id The id to look up.
 */
export const findResource = (resource: Resource, id: string): Resource | null => {

    if (resource.id === id) {
        return resource;
    }

    for (const r of resource.relations) {
        const find = findResource(r, id);

        if (find) {
            return find;
        }
    }

    return null;
}