describe('Load resources and relations', () => {

  it('Should display an Error 404 when we try to fetch invalid data', () => {
    cy.visit('/');

    cy.get('[data-cy="url-input"]').type("invalid string");
    cy.get('[data-cy="execute"]').contains("Execute").click();

    cy.get('[data-cy="error_snackbar"]').contains("Request failed with status code 404");
  });

  it('Should fetch a user resource and load its relations', () => {
    cy.fixture('user.json').as('user')
    cy.fixture('user_pets.json').as('userPets')
    cy.fixture('user_address.json').as('userAddress')
    cy.fixture('country.json').as('country')

    cy.intercept('GET', 'http://api/users/1', {fixture: 'user.json'});
    cy.intercept('GET', 'http://api/users/1/pets', {fixture: 'user_pets.json'});
    cy.intercept('GET', 'http://api/users/1/address', {fixture: 'user_address.json'});
    cy.intercept('GET', 'http://api/countries/1', {fixture: 'country.json'});

    cy.visit('/');

    // Fetch resource "user"
    cy.get('[data-cy="url-input"]').type("http://api/users/1");
    cy.get('[data-cy="execute"]').contains("Execute").click();

    // Node root should appear.
    cy.get('div[data-id="root"]').should('exist');

    // Load user relation "address"
    cy.get('[data-cy="address"]').contains('address').click();

    // Node root->address should appear.
    cy.get('div[data-id="root->address"]').should('exist');

    // Load user relation "pets"
    cy.get('[data-cy="pets"]').contains('pets').click();

    // Node root->pets should appear.
    cy.get('div[data-id="root->pets"]').should('exist');

    // Load address relation "country"
    cy.get('[data-cy="country"]').contains('country').click();

    // Node root->pets should appear.
    cy.get('div[data-id="root->address->country"]').should('exist');
  });

})